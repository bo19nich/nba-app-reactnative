import Moment from 'moment'
import React, { Component } from 'react'
import { Image, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { connect } from 'react-redux'
import { getGames } from '../../store/actions/gamesActions'

class Games extends Component {
    componentDidMount() {
        this.props.getGames()
    }
    showGames = (list) => {
        return list.games && list.games.map((item, i) => (
            <TouchableOpacity
                onPress={() => this.props.navigation.navigate('GamesArticle', { ...item })}
                key={i}
            >
                <View style={styles.gameContainer}>
                    <View style={styles.gameBox}>
                        <Image
                            source={item.away.logo}
                            style={{ width: 80, height: 80 }}
                            resizeMode='contain'
                        />
                        <Text style={styles.itemRecord}> {item.away.wins} - {item.away.loss}</Text>
                    </View>
                    <View style={styles.gameBox}>
                        <Text style={styles.gameTime}>{item.time}</Text>
                        <Text>{Moment(item.date).format('d MMMM')}</Text>
                    </View>
                    <View style={styles.gameBox}>
                        <Image
                            source={item.local.logo}
                            style={{ width: 80, height: 80 }}
                            resizeMode='contain'
                        />
                        <Text style={styles.itemRecord}> {item.local.wins} - {item.local.loss}</Text>
                    </View>
                </View>
            </TouchableOpacity>
        ))
    }
    render() {
        return (
            <ScrollView style={{ backgroundColor: '#f0f0f0' }}>
                <View
                    style={{
                        flex: 1,
                        flexDirection: 'column',
                        flexWrap: 'nowrap'
                    }}
                >
                    {this.showGames(this.props.Games)}
                </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    gameContainer: {
        flexDirection: 'row',
        backgroundColor: '#fff',
        marginBottom: 10,
        shadowColor: '#dddddd',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 1,
        borderRadius: 2
    },
    gameBox: {
        width: '33.3%',
        height: 100,
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center'
    },
    itemRecord: {
        fontFamily: 'Ionicons',
        fontSize: 12,
    },
    gameTime: {
        fontSize: 15,
        fontFamily: 'FontAwesome5_Solid',
    }
})

function mapStateToProps(state) {
    return {
        Games: state.Games
    }
}

export default connect(mapStateToProps, { getGames })(Games)
