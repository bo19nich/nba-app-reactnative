import React, { Component } from 'react'
import { ActivityIndicator, Button, ScrollView, StyleSheet, Text, View } from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'
import Video from 'react-native-video'
import { connect } from 'react-redux'
import { autoSignIn } from '../../store/actions/userActions'
import { getTokens, setTokens } from '../../utils/firebaseConfigs'

class GamesArticle extends Component {
    state = {
        isAuth: true,
        isLoading: true
    }

    manageState=(isLoading, isAuth)=>{
        this.setState({
            isAuth,
            isLoading
        })
    }

    componentDidMount() {
        const User = this.props.User
        getTokens(async (values) => {
            if (values[0][1] === null) {
                this.manageState(false, false)
            } else {
                await this.props.autoSignIn(values[1][1])
                if (!this.props.User.auth.token) {
                    this.manageState(false, false)
                } else {
                    setTokens(this.props.User.auth, () => {
                        this.manageState(false, true)
                    })
                }
            }
        })
    }

    render() {
        const params = this.props.navigation.state.params
        if (this.state.isLoading) {
            return (
                <View style={styles.loading}>
                    <ActivityIndicator />
                </View>
            )
        } else {
            return (
                <ScrollView style={{ backgroundColor: '#f0f0f0' }}>
                    {
                        this.state.isAuth ?
                            <View>
                                <Video
                                source={{uri:params.play}}
                                muted={true}
                                paused={true}
                                controls={true}
                                style={{width:'100%',height:250}}
                                />
                            </View>
                            :
                            <View style={styles.notAuth}>
                                <Icon name='md-sad' size={80} color='#d5d5d5' />
                                <Text>
                                    You are not logged in. Please Sign in first to see the video.
                        </Text>
                                <Button
                                    title='Login / Sign up'
                                    onPress={() => this.props.navigation.navigate('Auth')}
                                />
                            </View>
                    }
                </ScrollView>
            )
        }
    }
}

const styles = StyleSheet.create({
    loading: {
        flex: 1,
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center'
    },
    notAuth: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    notAuthText: {
        fontFamily: 'FontAwesome5_Solid'
    }
})

function mapStateToProps(state) {
    return {
        User: state.User
    }
}

export default connect(mapStateToProps, {autoSignIn})(GamesArticle)
