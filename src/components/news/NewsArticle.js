import Moment from 'moment'
import React, { Component } from 'react'
import { Image, ScrollView, StyleSheet, Text, View } from 'react-native'
import CoverImage from '../../assets/images/download1.jpg'

class NewsArticle extends Component {

    formatText = (content) => {
        const text = content.replace(/<p>/g, '').replace(/<\/p>/g, '')
        return text
    }

    render() {
        const params = this.props.navigation.state.params

        return (
            <ScrollView style={{ backgroundColor: '#f0f0f0' }}>
                <Image
                    source={CoverImage}
                    resizeMode='cover'
                    style={{ height: 250, width: '100%' }}
                />
                <View style={styles.articleContainer}>
                    <View>
                        <Text style={styles.articleTitle}>
                            {params.title}
                        </Text>
                        <Text style={styles.articleData}>
                            {params.team} - Posted on {Moment(params.date).format('d MMMM')}
                        </Text>
                    </View>
                    <View style={styles.articleContent}>
                        <Text style={styles.articleContentText}>
                            {this.formatText(params.content)}
                        </Text>
                    </View>
                </View>

            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    articleContainer: {
        padding: 10
    },
    articleTitle: {
        fontSize: 23,
        color: '#323232',
        fontFamily: 'FontAwesome5_Solid'
    },
    articleData: {
        fontSize: 12,
        color: '#828282',
        fontFamily: 'FontAwesome5_Regular'
    },
    articleContent: {
        marginTop: 10
    },
    articleContentText: {
        fontSize: 14,
        lineHeight: 20,
        fontFamily: 'FontAwesome5_Regular'
    }
})

export default NewsArticle
