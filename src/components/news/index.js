import Moment from 'moment'
import React, { Component } from 'react'
import { Image, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { connect } from 'react-redux'
import placeHolderImg from '../../assets/images/desk.jpg'
import { getNews } from '../../store/actions/newsAction '

class News extends Component {

    componentDidMount() {
        this.props.getNews()
    }

    renderArticle = (news) => {

        return news && news.map((item, i) => {
            return (
                <TouchableOpacity
                    key={i}
                    onPress={() => this.props.navigation.navigate('NewsArticle', {
                        ...item
                    })}
                >
                    <View style={styles.cardContainer}>
                        <View>
                            <Image
                                style={{ width: '100%', height: 150, justifyContent: 'space-around' }}
                                resizeMode='cover'
                                source={placeHolderImg} />
                        </View>
                        <View style={styles.contentCard}>
                            <Text style={styles.titleCard}>{item.title}</Text>
                            <View style={styles.bottomCard}>
                                <Text style={styles.bottomCardTeam}>{item.team} - </Text>
                                <Text style={styles.bottomCardText}> Posted at {Moment(item.date).format('d MMMM')} </Text>
                            </View>
                        </View>
                    </View>
                </TouchableOpacity>
            )
        }
        )
    }

    render() {
        return (
            <ScrollView style={{ backgroundColor: "#f0f0f0" }}>
                {this.renderArticle(this.props.News.articles)}
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    cardContainer: {
        backgroundColor: '#fff',
        margin: 10,
        shadowColor: '#dddddd',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 1,
        borderRadius: 2
    },
    contentCard: {
        borderWidth: 1,
        borderColor: '#dddddd'
    },
    titleCard: {
        fontFamily: 'FontAwesome5_Solid',
        fontSize: 16,
        padding: 10,
        color: '#232323'
    },
    bottomCard: {
        flex: 1,
        flexDirection: 'row',
        padding: 10,
        borderTopWidth: 1,
        borderTopColor: '#e6e6e6'
    },
    bottomCardTeam: {
        fontFamily: 'FontAwesome5_Solid',
        color: '#828282',
        fontSize: 12
    },
    bottomCardText: {
        fontFamily: 'FontAwesome5_Regular',
        color: '#828282',
        fontSize: 12
    }
})

function mapStateToProps(state) {
    return {
        News: state.News
    }
}

export default connect(mapStateToProps, { getNews })(News)
