import React from 'react'
import { Image, View } from 'react-native'
import Logo from '../../assets/images/nba_login_logo.png'

const authLogo = () => {
    return (
        <View style={{ alignItems: 'center' }}>
            <Image
                source={Logo}
                resizeMode='center'
                style={{
                    width: 170,
                    height: 150
                }} />
        </View>
    )
}

export default authLogo
