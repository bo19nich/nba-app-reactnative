import React, { Component } from 'react'
import { Button, Platform, StyleSheet, Text, View } from 'react-native'
import { connect } from 'react-redux'
import { signIn, signUp } from '../../store/actions/userActions'
import { setTokens } from '../../utils/firebaseConfigs'
import FormInput from '../../utils/forms/input'
import validationRules from '../../utils/forms/validationRules'

class AuthForm extends Component {
    state = {
        type: 'Login',
        action: 'Login',
        actionMode: 'I want to Register',
        hasErrors: false,
        form: {
            email: {
                value: '',
                valid: false,
                type: 'textInput',
                rules: {
                    isRequired: true,
                    isEmail: false
                }
            },
            password: {
                value: '',
                valid: false,
                type: 'textInput',
                rules: {
                    isRequired: true,
                    minLength: 6
                }
            },
            confirmPassword: {
                value: '',
                valid: false,
                type: 'textInput',
                rules: {
                    confirmPass: 'password'
                }
            }
        }
    }

    submitForm = async () => {
        let isFormValid = true
        let formToSubmit = {}
        const formCopy = this.state.form

        for (let key in formCopy) {
            if (this.state.type === 'Login') {
                // LOGIN
                if (key !== 'confirmPassword') {
                    isFormValid = isFormValid && formCopy[key].valid
                    formToSubmit[key] = formCopy[key].value
                }
            } else {
                // REGISTER
                isFormValid = isFormValid && formCopy[key].valid
                formToSubmit[key] = formCopy[key].value
            }
        }
        if (isFormValid) {
            if (this.state.type === 'Login') {
                this.props.signIn(formToSubmit)
                this.manageAccess()
            } else {
                this.props.signUp(formToSubmit)
            }
        } else {
            this.setState({
                hasErrors: true
            })
        }
    }

    manageAccess = () => {
        if (!this.props.User.auth.uid) {
            this.setState({ hasErrors: true })
        } else {
            setTokens(this.props.User.auth, () => {
                this.setState({ hasErrors: false })
                this.props.goNext()
            })
        }
    }

    formHasErrors = () => (
        this.state.hasErrors ?
            <View style={styles.errorContainer}>
                <Text style={styles.errorText}>Oops!, Invalid credentials.</Text>
            </View>
            : null
    )

    confirmPassword = () => (
        this.state.type !== 'Login' ?
            <FormInput
                placeholder='Confirm your password'
                placeholderTextColor='#cecece'
                type={this.state.form.confirmPassword.type}
                value={this.state.form.confirmPassword.value}
                onChangeText={value => this.updateInput("confirmPassword", value)}
                secureTextEntry
            // overrideStyle={{}}
            /> :
            null
    )

    updateInput = (name, value) => {
        this.setState({
            hasErrors: false
        })
        let formCopy = this.state.form
        formCopy[name].value = value

        let rules = formCopy[name].rules
        let valid = validationRules(rules, value, formCopy)
        formCopy[name].valid = valid
        this.setState({
            form: formCopy
        })
    }

    changeActionMode = () => {
        const type = this.state.type
        this.setState({
            type: type === 'Login' ? 'Register' : 'Login',
            action: type === 'Login' ? 'Register' : 'Login',
            actionMode: type === 'Login' ? 'I want to Login' : 'I want to register',
        })
    }
    render() {
        return (
            <View>
                <FormInput
                    placeholder='Enter email'
                    placeholderTextColor='#cecece'
                    type={this.state.form.email.type}
                    value={this.state.form.email.value}
                    autoCapitalize="none"
                    keyboardType="email-address"
                    onChangeText={value => this.updateInput("email", value)}
                // overrideStyle={{}}
                />
                <FormInput
                    placeholder='Enter your password'
                    placeholderTextColor='#cecece'
                    type={this.state.form.password.type}
                    value={this.state.form.password.value}
                    onChangeText={value => this.updateInput("password", value)}
                    secureTextEntry
                // overrideStyle={{}}
                />
                {this.confirmPassword()}
                {this.formHasErrors()}
                <View style={{ marginTop: 20 }}>
                    <View style={styles.button}>
                        <Button
                            title={this.state.action}
                            onPress={this.submitForm} />
                    </View>
                    <View style={styles.button}>
                        <Button
                            title={this.state.actionMode}
                            onPress={this.changeActionMode} />
                    </View>
                    <View style={styles.button}>
                        <Button
                            title="I'll do it later"
                            onPress={this.props.goNext} />
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    errorContainer: {
        marginBottom: 10,
        marginTop: 30,
        padding: 10,
        backgroundColor: '#f44336'
    },
    errorText: {
        color: '#fff',
        textAlignVertical: 'center',
        textAlign: 'center'
    },
    button: {
        ...Platform.select({
            ios: {},
            android: {
                marginBottom: 10,
                marginTop: 10
            }
        })
    }
})

function mapStateToProps(state) {
    return {
        User: state.User
    }
}

export default connect(mapStateToProps, { signIn, signUp })(AuthForm)
