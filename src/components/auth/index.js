import React, { Component } from 'react'
import { ActivityIndicator, ScrollView, StyleSheet, View } from 'react-native'
import { connect } from 'react-redux'
import { autoSignIn } from '../../store/actions/userActions'
import { getTokens, setTokens } from '../../utils/firebaseConfigs'
import AuthForm from './AuthForm'
import AuthLogo from './authLogo'


class Auth extends Component {
    state = {
        isLoading: true
    }
    componentDidMount() {
        // AsyncStorage.clear()
        getTokens(async (values) => {
            if (values[0][1] === null) {
                this.setState({ isLoading: false })
            } else {
                await this.props.autoSignIn(values[1][1])
                if (!this.props.User.auth.token) {
                    this.setState({ isLoading: false })
                } else {
                    setTokens(this.props.User.auth, () => {
                        this.goNext()
                    })
                }
            }
        })
    }

    goNext = () => {
        this.props.navigation.navigate('App')
    }
    render() {
        if (this.state.isLoading) {
            return (
                <View style={styles.loading}>
                    <ActivityIndicator />
                </View>
            )
        } else {
            return (
                <ScrollView style={styles.container}>
                    <View>
                        <AuthLogo />
                        <AuthForm
                            goNext={this.goNext} />
                    </View>
                </ScrollView>
            )
        }
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#1b428a',
        padding: 50
    },
    loading: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center'
    }
})

function mapStateToProps(state) {
    return {
        User: state.User
    }
}

export default connect(mapStateToProps, { autoSignIn })(Auth)
