// import Video from './video.mp4'

export const games = [
    {
        id: "-L_JF2LaIZHxUSwuMZKF",
        away: "-L_J9f5AuDFNKjFI3EUN",
        date: "2019-01-02T00:00:00",
        local: "-L_J9exs88WcMW7_s0Oo",
        play: "https://drive.google.com/open?id=1YoqtroJOS0EJ8pibswrfyz0vXB4vutIc",
        time: "7:00 pm ET"
    },
    {
        id: "-L_JF2TRp5Dx7mceg81c",
        away: "-L_J9fFfjWSLpnllnE6o",
        date: "2019-01-02T00:00:00",
        local: "-L_J9f8vZnmztVkC5aEa",
        play: "https://drive.google.com/open?id=1YoqtroJOS0EJ8pibswrfyz0vXB4vutIc",
        time: "7:30 pm ET"
    },
    {
        id: "-L_JF2TRp5Dx7mceg81d",
        away: "-L_J9fOGwaCVg-swrbxt",
        date: "2019-01-02T00:00:00",
        local: "-L_J9fKNJ8gJeI6K8EDJ",
        play: "https://drive.google.com/open?id=1YoqtroJOS0EJ8pibswrfyz0vXB4vutIc",
        time: "8:30 pm ET"
    },
    {
        id: "-L_Jm2VpRGck2_pEVdp1",
        away: "-L_J9fC1RVVe609VsnSK",
        date: "2019-01-02T00:00:00",
        local: "-L_J9f5AuDFNKjFI3EUN",
        play: "https://drive.google.com/open?id=1YoqtroJOS0EJ8pibswrfyz0vXB4vutIc",
        time: "8:30 pm ET"
    },
    {
        id: "-L_Jm2cVmaR1KspmROCr",
        away: "-L_J9fOGwaCVg-swrbxu",
        date: "2019-01-02T00:00:00",
        local: "-L_J9fJcR1k8BTTkT0mr",
        play: "https://drive.google.com/open?id=1YoqtroJOS0EJ8pibswrfyz0vXB4vutIc",
        time: "9:00 pm ET"
    },
    {
        id: "-L_Jm2d2NMgKyDTc7eNB",
        away: "-L_J9fS04QBeqvPtJZyE",
        date: "2019-01-02T00:00:00",
        local: "-L_J9fRAxp5tK3EKKeyv",
        play: "https://drive.google.com/open?id=1YoqtroJOS0EJ8pibswrfyz0vXB4vutIc",
        time: "9:30 pm ET"
    }
]