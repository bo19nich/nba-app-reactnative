import Timberwolves from './nba-logos/1200px-Minnesota_Timberwolves_logo.svg.png'
import Knicks from './nba-logos/1200px-New_York_Knicks_logo.svg.png'
import Trail_Blazer from './nba-logos/1200px-Portland_Trail_Blazers_logo.svg.png'
import Celtics from './nba-logos/Boston-Celtics-logo.png'
// import Bulls from './nba-logos/Chicago-Bulls-logo.png'
import Kings from './nba-logos/Chicago-Bulls-logo.png'
import Mavericks from './nba-logos/Dallas-Mavericks-Logo.png'
import Cavaliers from './nba-logos/download.png'
import Hawks from './nba-logos/hawks.png'
import Hornets from './nba-logos/hornest.png'
import Houston from './nba-logos/houston_rockets.png'
import Clippers from './nba-logos/Los-Angeles-Clippers-Logo.png'
import Lakers from './nba-logos/Los-Angeles-Lakers-Logo.png'
import memphis from './nba-logos/memphis.png'
import miani from './nba-logos/miani.png'
import Bucks from './nba-logos/Milwaukee-Bucks-logo.png'
import Nuggets from './nba-logos/nuggets.png'
import Magic from './nba-logos/Orlando-Magic-logo.png'
import Pacers from './nba-logos/Pacers.png'
import p_76ers from './nba-logos/philadelphia_76ers.png'
import Suns from './nba-logos/phoenix-suns-logo-vector.png'
import Pistons from './nba-logos/Pistons.png'
import Raptors from './nba-logos/Raptors.png'
import Spurs from './nba-logos/San-Antonio-Spurs-logo.png'
import Jazz from './nba-logos/Utah-Jazz-logo.png'
import Warriors from './nba-logos/Warriors.png'
import Wizards from './nba-logos/Washington-Wizards-logo.png'

export const teams = [
    {
        id: "-L_J9exs88WcMW7_s0Oo",
        city: "New York",
        logo: Knicks,
        loss: 13,
        name: "Knicks",
        wins: 51
    },
    {
        id: "-L_J9f0aNbgK75FvfIzE",
        city: "Cleveland",
        logo: Cavaliers,
        loss: 18,
        name: "Cavaliers",
        wins: 24
    },
    {
        id: "-L_J9f4LWw8vQv049anl",
        city: "Boston",
        logo: Celtics,
        loss: 35,
        name: "Celtics",
        wins: 18
    },
    {
        id: "-L_J9f4_Mh0j8lNUH9Bt",
        city: "Philadelphia",
        logo: p_76ers,
        loss: 41,
        name: "76ers",
        wins: 23
    },
    {
        id: "-L_J9f4rCM1ljEyxxQDs",
        city: "Milwakee",
        logo: Bucks,
        loss: 48,
        name: "Bucks",
        wins: 16
    },
    {
        id: "-L_J9f5AuDFNKjFI3EUN",
        city: "Chicago",
        logo: Mavericks,
        loss: 16,
        name: "Bulls",
        wins: 47
    },
    {
        id: "-L_J9f87VZGGdbaY0x--",
        city: "Los Angeles",
        logo: Clippers,
        loss: 23,
        name: "Clippers",
        wins: 40
    },
    {
        id: "-L_J9f8IL4QI5ZljnoVD",
        city: "Vancouver",
        logo: memphis,
        loss: 12,
        name: "Grizzlies",
        wins: 50
    },
    {
        id: "-L_J9f8vZnmztVkC5aE_",
        city: "Miami",
        logo: miani,
        loss: 12,
        name: "Heat",
        wins: 53
    },
    {
        id: "-L_J9f8vZnmztVkC5aEa",
        city: "Atlanta",
        logo: Hawks,
        loss: 26,
        name: "Hawks",
        wins: 23
    },
    {
        id: "-L_J9fBrdWnevIZ2OlhI",
        city: "Charlotte",
        logo: Hornets,
        loss: 43,
        name: "Hornets",
        wins: 12
    },
    {
        id: "-L_J9fC1RVVe609VsnSK",
        city: "Charlotte",
        logo: Jazz,
        loss: 26,
        name: "Jazz",
        wins: 40
    },
    {
        id: "-L_J9fCinHyNlJsJHOi4",
        city: "Sacramento",
        logo: Kings,
        loss: 16,
        name: "Kings",
        wins: 30
    },
    {
        id: "-L_J9fCinHyNlJsJHOi5",
        city: "Los Angeles",
        logo: Lakers,
        loss: 14,
        name: "Lakers",
        wins: 34
    },
    {
        id: "-L_J9fFfjWSLpnllnE6o",
        city: "Orlando",
        logo: Magic,
        loss: 47,
        name: "Magic",
        wins: 12
    },
    {
        id: "-L_J9fFovY_IJtYHVhbh",
        city: "Dallas",
        logo: Mavericks,
        loss: 12,
        name: "Mavericks",
        wins: 60
    },
    {
        id: "-L_J9fGaivdQpi7ruy7n",
        city: "Brooklyn",
        logo: Mavericks,
        loss: 48,
        name: "Nets",
        wins: 12
    },
    {
        id: "-L_J9fGaivdQpi7ruy7o",
        city: "Denver",
        logo: Nuggets,
        loss: 29,
        name: "Nuggets",
        wins: 40
    },
    {
        id: "-L_J9fJWOtzf2iQgzhCr",
        city: "Indiana",
        logo: Pacers,
        loss: 29,
        name: "Pacers",
        wins: 50
    },
    {
        id: "-L_J9fJcR1k8BTTkT0mr",
        city: "Detroit",
        logo: Pistons,
        loss: 14,
        name: "Pistons",
        wins: 48
    },
    {
        id: "-L_J9fKNJ8gJeI6K8EDJ",
        city: "Toronto",
        logo: Raptors,
        loss: 19,
        name: "Raptors",
        wins: 27
    },
    {
        id: "-L_J9fKNJ8gJeI6K8EDK",
        city: "Houston",
        logo: Houston,
        loss: 17,
        name: "Rockets",
        wins: 72
    },
    {
        id: "-L_J9fNFwyGACTgz_axp",
        city: "San antonio",
        logo: Spurs,
        loss: 24,
        name: "Spurs",
        wins: 52
    },
    {
        id: "-L_J9fNROg9tChdUsgTQ",
        city: "Phoenix",
        logo: Suns,
        loss: 34,
        name: "Suns",
        wins: 12
    },
    {
        id: "-L_J9fOGwaCVg-swrbxt",
        city: "OKC",
        logo: Suns,
        loss: 14,
        name: "Thunder",
        wins: 62
    },
    {
        id: "-L_J9fOGwaCVg-swrbxu",
        city: "Minnesota",
        logo: Timberwolves,
        loss: 44,
        name: "Timberwolves",
        wins: 22
    },
    {
        id: "-L_J9fQzR-YzGNa7C57W",
        city: "Portland",
        logo: Trail_Blazer,
        loss: 13,
        name: "Trail Blazers",
        wins: 52
    },
    {
        id: "-L_J9fRAxp5tK3EKKeyv",
        city: "Golden State",
        logo: Warriors,
        loss: 33,
        name: "Warriors",
        wins: 42
    },
    {
        id: "-L_J9fS04QBeqvPtJZyE",
        city: "Washington",
        logo: Wizards,
        loss: 13,
        name: "Wizards",
        wins: 52
    }
]