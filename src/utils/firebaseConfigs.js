import AsyncStorage from '@react-native-community/async-storage';

export const FIREBASE_DB_URL = `https://nba-app-63d4b.firebaseio.com`
export const APIKEY = `AIzaSyDHIMc9-WxdvNHhEuHNUZlkZKiOkAGulf4`
export const SIGNIN_URL = `https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=${APIKEY}`
export const SIGNUP_URL = `https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=${APIKEY}`
export const REFRESH = `https://securetoken.googleapis.com/v1/token?key=${APIKEY}`

export const getTokens = cb => {
  AsyncStorage.multiGet([
    '@nba_app@token',
    '@nba_app@refreshToken',
    '@nba_app@uid',
    '@nba_app@expirationTime',
  ]).then(values => {
    cb(values)
  })
}

export const setTokens = (values, cb) => {
  const dateNow = new Date()
  const expiration = dateNow.getTime() + (3600 * 1000)
  AsyncStorage.multiSet([
    ['@nba_app@token', values.token],
    ['@nba_app@refreshToken', values.refreshToken],
    ['@nba_app@uid', values.uid],
    ['@nba_app@expirationTime', expiration.toString()],
  ]).then(res => {
    cb()
  })
}

    // apiKey: "AIzaSyDHIMc9-WxdvNHhEuHNUZlkZKiOkAGulf4",
    // authDomain: "nba-app-63d4b.firebaseapp.com",
    // databaseURL: "https://nba-app-63d4b.firebaseio.com",
    // projectId: "nba-app-63d4b",
    // storageBucket: "nba-app-63d4b.appspot.com",
    // messagingSenderId: "630728310196",
    // appId: "1:630728310196:web:532f1c368dd06f91ac46e1"