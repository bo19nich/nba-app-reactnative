import axios from 'axios'
import { REFRESH, SIGNIN_URL, SIGNUP_URL } from '../../utils/firebaseConfigs'
import { AUTOSIGNIN, SIGNIN, SIGNUP } from '../types'

export const autoSignIn = token => {
    return async dispatch => {
        const res = await axios.post(REFRESH, {
            grant_type: 'refresh_token',
            refresh_token: token
        })
        try {
            dispatch({
                type: AUTOSIGNIN,
                payload: res.data
            })
        } catch (error) {
            console.log(error)
        }
    }
}

export const signIn = (data) => {
    return async dispatch => {
        const res = await axios.post(SIGNIN_URL, {
            email: data.email,
            password: data.password,
            returnSecureToken: true
        }, {
            header: { "Content-Type": "application/json" }
        })
        try {
            dispatch({
                type: SIGNIN,
                payload: res.data
            })
        } catch (error) {
            console.log(error)
        }
    }
}
export const signUp = (data) => {
    return async dispatch => {
        const res = await axios.post(SIGNUP_URL, {
            email: data.email,
            password: data.password,
            returnSecureToken: true
        }, {
            header: { "Content-Type": "application/json" }
        })
        try {
            dispatch({
                type: SIGNUP,
                payload: res.data
            })
        } catch (error) {
            console.log(error)
        }
    }
}