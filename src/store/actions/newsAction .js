// import axios from 'axios'
// import { FIREBASE_DB_URL } from '../../utils/firebaseConfigs'
import { news } from '../../utils/data/newsData'
import { GET_NEWS } from '../types'

export const getNews = () => {
    return async dispatch => {
        // const res = await axios.get(`${FIREBASE_DB_URL}/news.json`)
        try {
            dispatch({
                type: GET_NEWS,
                payload: news
            })
        } catch (error) {
            console.warn(error)
        }
    }
}