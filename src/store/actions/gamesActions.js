import { games } from '../../utils/data/gamesData'
import { teams } from '../../utils/data/teamsData'
import { GET_GAMES } from '../types'

export const getGames = () => {
    return dispatch => {

        const newGames = games.map(game => {
            game.away = teams.find(team => {
                return team.id === game.away
            })
            game.local = teams.find(team => {
                return team.id === game.local
            })
            return game
        })
        dispatch({
            type: GET_GAMES,
            payload: {
                games: newGames
            }
        })
    }
}