import { combineReducers } from 'redux'
import Games from './games_reducer'
import News from './news_reducer'
import User from './users_reducer'

const rootReducer = combineReducers({
    User,
    News,
    Games
})

export default rootReducer