import { AUTOSIGNIN, SIGNIN, SIGNUP } from '../types'
export default (state = {}, action) => {
    switch (action.type) {
        case SIGNIN:
            return {
                ...state,
                auth: {
                    uid: action.payload.localId || false,
                    token: action.payload.idToken || false,
                    refreshToken: action.payload.refreshToken || false
                }
            }
        case AUTOSIGNIN:
            return {
                ...state,
                auth: {
                    uid: action.payload.user_id || false,
                    token: action.payload.id_token || false,
                    refreshToken: action.payload.refresh_token || false
                }
            }
        case SIGNUP:
            return {
                ...state,
                auth: {
                    uid: action.payload.localId || false,
                    token: action.payload.idToken || false,
                    refreshToken: action.payload.refreshToken || false
                }
            }
        default:
            return state
    }
}