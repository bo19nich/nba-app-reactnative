import React from 'react';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';
// SCREENS
import SignIn from './components/auth';
import Games from './components/games';
import GamesArticle from './components/games/GamesArticle';
import News from './components/news';
import NewsArticle from './components/news/NewsArticle';
import LogoComponent from './utils/LogoComponent';


const headerConf = {
    headerLayoutPreset: 'center',
    defaultNavigationOptions: {
        headerStyle: {
            backgroundColor: "#001338"
        },
        headerTintColor: "#fff",
        headerTitle: LogoComponent
    }
}

const NewsStack = createStackNavigator({
    News,
    NewsArticle
}, headerConf)

const GameStack = createStackNavigator({
    Games,
    GamesArticle
}, headerConf)

const AppStack = createBottomTabNavigator({
    News: NewsStack,
    Games: GameStack
}, {
    tabBarOptions: {
        activeTintColor: '#fff',
        showLabel: false,
        activeBackgroundColor: '#00194b',
        inactiveBackgroundColor: '#001338',
        style: {
            backgroundColor: '#001338'
        },
    },
    initialRouteName: "News",
    defaultNavigationOptions: ({ navigation }) => ({
        tabBarIcon: ({ focused, horizontal, tintColor }) => {
            const { routeName } = navigation.state
            let iconName
            if (routeName === 'News') {
                iconName = `ios-basketball`
            } else if (routeName === 'Games') {
                iconName = `md-tv`
            }
            return <Ionicons name={iconName} size={25} color={tintColor} />
        }
    })
})

const AuthStack = createStackNavigator({
    SignIn: SignIn
}, {
    headerMode: "none"
})

export const RootNavigation = () => {
    return createAppContainer(createSwitchNavigator({
        App: AppStack,
        Auth: AuthStack
    }, {
        initialRouteName: 'Auth'
    }))
}
