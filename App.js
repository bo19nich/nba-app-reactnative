import React, { Component } from 'react';
import { StyleSheet, View } from 'react-native';
import { RootNavigation } from './src/routes';

class App extends Component {

  render() {
    const Nav = RootNavigation()
    return (
      <View style={styles.container}>
        <Nav />
      </View>
    );
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  }
});

export default App;
