import React from 'react';
import { AppRegistry } from 'react-native';
import 'react-native-gesture-handler';
import { Provider } from 'react-redux';
import { applyMiddleware, createStore } from 'redux';
import reduxThunk from 'redux-thunk';
import App from './App';
import { name as appName } from './app.json';
import reducers from './src/store/reducers';

// const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

const createStoreWithRedux = createStore(reducers, applyMiddleware(reduxThunk))
const appRedux = () => (
    <Provider store={createStoreWithRedux}>
        <App />
    </Provider>
)

AppRegistry.registerComponent(appName, () => appRedux);
